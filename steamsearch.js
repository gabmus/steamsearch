#!/usr/bin/env node

const DB_URL = 'http://api.steampowered.com/ISteamApps/GetAppList/v0002/'
const DB_PATH = './steam_db.json';
const fs = require('fs');
const Table = require('cli-table');

const table = new Table({
    head: ['Name', 'AppID']
});

if (process.argv.length < 3) {
    console.log(`Usage: ${process.argv[0]} ${process.argv[1]} "search terms"`);
    process.exit(1);
}

var search_term = process.argv[2];

var do_search = () => {
    fs.readFile(DB_PATH, 'utf-8', (err, content) => {
        let db = JSON.parse(content);
        let found = false;
        db.applist.apps.forEach((val, index, arr) => {
            if (val.name.toLowerCase().includes(search_term.toLowerCase())) {
                table.push([val.name, val.appid]);
                found = true;
            }
        });
        if (found) {
            console.log(table.toString());
            process.exit();
        }
        else {
            console.error('No results found');
            process.exit(1);
        }
    });
}

if (!fs.existsSync(DB_PATH)) {
    console.log(`${DB_PATH} does not exist. Downloading...`);
    var http = require('http');
    let file = fs.createWriteStream(DB_PATH);
    let req = http.get(DB_URL, res => {
        res.pipe(file);
        file.on('finish', () => {
            file.close(do_search);
        }).on('error', err => {
            fs.unlink(DB_PATH);
            console.error(err);
        });
    });
}

else do_search();
