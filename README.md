# SteamSearch

Simple CLI app, search for steam games by name and get a list of matches with respective appid.

# Requirements

You need to have `node` installed.

# Usage

```bash
git clone https://gitlab.com/gabmus/steamsearch
cd steamsearch
npm install
node steamsearch.js "search terms"
```

To manually update `steam_db.json` run `bash update_steam_db.sh`.
